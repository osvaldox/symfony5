<?php

namespace App\Entity;

use App\Repository\CiudadRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CiudadRepository::class)
 */
class Ciudad
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcionCiudad;

    /**
     * @ORM\ManyToOne(targetEntity=Provincia::class, inversedBy="ciudades")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idProvincia;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcionCiudad(): ?string
    {
        return $this->descripcionCiudad;
    }

    public function setDescripcionCiudad(string $descripcionCiudad): self
    {
        $this->descripcionCiudad = $descripcionCiudad;

        return $this;
    }

    public function getIdProvincia(): ?Provincia
    {
        return $this->idProvincia;
    }

    public function setIdProvincia(?Provincia $idProvincia): self
    {
        $this->idProvincia = $idProvincia;

        return $this;
    }
}
